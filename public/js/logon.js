$(document).ready(function() {
	
  var citys = new Array("北京","宁波");

  var beijingBuilding =new Array("环宇","钻石","盘古");

  var ningboBuilding = new Array("宁波A","宁波B");

  $("#register").on("click",function(){
  	$(location).attr('href', 'register.html');  
  });

  $("#logon").on("click",function(){
    var email = $("#name").val();
    var password = $("#password").val();
    $.ajax({
      url: "/api/book/logon",
      type: "POST",
      dataType: "json",
      data: {email: email,password: password},
      cache: true
      })
    .done(function(user) {
    if(user.result === 1){
      alert("This username or password is wrong.");
    }
    else if(user.result === 2){
      alert("The user is not activated.");
    }
    else{
    $.cookie('email', user.email, { expires: 10000, path: '/' }); //expires after 10000 days
    $.cookie('activecode', user.activecode, { expires: 10000, path: '/' }); //expires after 10000 days
    $.cookie('city', user.city, { expires: 10000, path: '/' }); //expires after 10000 days
    $.cookie('building', user.building, { expires: 10000, path: '/' }); //expires after 10000 days
    $.cookie('seat', user.seat, { expires: 10000, path: '/' }); //expires after 10000 days
    $.cookie('user_id',user._id,{ expires: 10000, path: '/' });//expires after 10000 days
    $(location).attr('href', 'index.html');  
    }
      })
    .error(function(err) {
        alert("Failed to create a User.");
      });
  });


  $("#registerSubmit").on('click',function(){
    var $email = $.trim($('#me_email').val());
  	if ($email === "") {
  		alert("Please input Intranet ID!");
  		return;
  	}
  	var $password = $.trim($('#password').val());
  	if ($password === "") {
  		alert("Please input the password!");
  		return;
  	}
  	var $passwordConfirm = $.trim($('#passwordConfirm').val());
  	if ($passwordConfirm === "") {
  		alert("Please input the confirm password!");
  		return;
  	}
  	if ($passwordConfirm !== $password) {
  		alert("Please make sure the password is consistent!");
  		return;
  	}
    var $city = $('#me_city').val();
    if($city === null){
      alert("Please choose your city!");
      return;
    }
    var $building = $('#me_building').val();
    if($building === null){
      alert("Please choose your building!");
      return;
    }
    var $seat = $.trim($('#me_seat').val());
    if ($seat === "") {
      alert("Please input seat number!");
      return;
    }
    

    $.ajax({
      url: "/api/book/register",
      type: "POST",
      dataType: "json",
      data: {email: $email,password: $password, city: $city, building: $building, seat: $seat},
      cache: true
      })
    .done(function(user) {
    if(user.result === 1){
    	alert("This Intranet ID has been registered.");
    }
    if(user.result === 0){
    	alert("This activety mail is sended to "+$email+".");
    }

      })
    .error(function(err) {
      	alert("Failed to create a User.");
      });
   });

  $("#me_city").on("change",function(){
    var $city = $('#me_city').val();
    var buiding;
    var buidings;
    var buidingstr="";
    if($city === "0"){
      buidings=beijingBuilding;
    }
    else if($city === "1"){
      buidings=ningboBuilding;
    }
    for(buiding in buidings){
      buidingstr+="<option value="+buiding+">"+buidings[buiding]+"</option>";
    }
    $('#me_building').html(buidingstr);
    $("#me_building").selectmenu('refresh', true);
  });
});

